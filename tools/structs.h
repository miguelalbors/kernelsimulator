#ifndef STRUCTS_H
#define STRUCTS_H

#include <sys/time.h>
#include <stdint.h>

#define S_STOPPED 0
#define S_WORKING 1

typedef struct
{
  unsigned int dV;
  unsigned int dF;
} PaginaRef;

typedef struct
{
  PaginaRef *data;
  int count;
  int size;
} PGB;

typedef struct
{
  int code, data;
  PGB pgb;
} MM;

typedef struct
{
  int reg[16]; //Registros
  int cc;
  int pc;
} Context;

typedef struct
{
  long int pid;
  int life;
  int priority;
  MM mm;
  struct timeval startedAt;
} Task;

struct node
{
   int data;
   Task task;
   Context context;
   struct node *next;
};

typedef struct
{
  Task task;
  int quantum;
  int state;

  Context context;
} CPU_Task;

typedef struct
{
  CPU_Task *data;
  int size;
} CPU_Task_List;

typedef struct
{
  Task *data;
  int front;
  int rear;
  int itemCount;
  unsigned int max;
} Queue;

typedef CPU_Task *Thread;
typedef Thread *Core;
typedef struct
{
  Core *cores;
  int thread_in_core;
} CPU;

typedef struct
{
  int R1, R2, R3, AAAAAA;
} Instruction;

typedef uint8_t Palabra;
typedef struct
{
  Palabra *data;
  int size;
} Memory;

typedef struct
{
  int pageAmount;
  int reserved;
} KernelData;


#endif //STRUCTS_H
