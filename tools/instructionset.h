#ifndef INSTRUCTIONSET_H
#define INSTRUCTIONSET_H

#include <stdio.h>
#include "structs.h"
#include "memory.h"

int SHOWINSTRUCTIONS = 0;

typedef void (*Instruction_Table)(CPU_Task *task, Instruction inst);

void i0 (CPU_Task *task, Instruction inst )
{
  //ld
  task->context.reg[inst.R1] = GetWord(inst.AAAAAA, &(task->task.mm.pgb));
  if(SHOWINSTRUCTIONS) printf("<%ld> ld %d %X\n", task->task.pid, inst.R1, inst.AAAAAA);
}
void i1 (CPU_Task *task, Instruction inst )
{
  //st
  SetWord(inst.AAAAAA, task->context.reg[inst.R1], &(task->task.mm.pgb));
  if(SHOWINSTRUCTIONS) printf("<%ld> st %d %X\n", task->task.pid, inst.R1, inst.AAAAAA);
}
void i2 (CPU_Task *task, Instruction inst )
{
  //add
  task->context.reg[inst.R1] = task->context.reg[inst.R2] + task->context.reg[inst.R3];
  if(SHOWINSTRUCTIONS) printf("<%ld> add %d %d %d\n", task->task.pid, inst.R1, inst.R2, inst.R3);
}
void i3 (CPU_Task *task, Instruction inst )
{
  //sub
  task->context.reg[inst.R1] = task->context.reg[inst.R2] - task->context.reg[inst.R3];
  if(SHOWINSTRUCTIONS) printf("<%ld> sub %d %d %d\n", task->task.pid, inst.R1, inst.R2, inst.R3);
}
void i4 (CPU_Task *task, Instruction inst )
{
  //mul
  task->context.reg[inst.R1] = task->context.reg[inst.R2] * task->context.reg[inst.R3];
  if(SHOWINSTRUCTIONS) printf("<%ld> mul %d %d %d\n", task->task.pid, inst.R1, inst.R2, inst.R3);
}
void i5 (CPU_Task *task, Instruction inst )
{
  //div
  if(task->context.reg[inst.R3] != 0)
  {
    task->context.reg[inst.R1] = task->context.reg[inst.R2] / task->context.reg[inst.R3];
  }
  if(SHOWINSTRUCTIONS) printf("<%ld> div %d %d %d\n", task->task.pid, inst.R1, inst.R2, inst.R3);
}
void i6 (CPU_Task *task, Instruction inst )
{
  //and
  task->context.reg[inst.R1] = task->context.reg[inst.R2] & task->context.reg[inst.R3];
  if(SHOWINSTRUCTIONS) printf("<%ld> and %d %d %d\n", task->task.pid, inst.R1, inst.R2, inst.R3);
}
void i7 (CPU_Task *task, Instruction inst )
{
  //or
  task->context.reg[inst.R1] = task->context.reg[inst.R2] | task->context.reg[inst.R3];
  if(SHOWINSTRUCTIONS) printf("<%ld> or %d %d %d\n", task->task.pid, inst.R1, inst.R2, inst.R3);
}
void i8 (CPU_Task *task, Instruction inst )
{
  //xor
  task->context.reg[inst.R1] = task->context.reg[inst.R2] ^ task->context.reg[inst.R3];
  if(SHOWINSTRUCTIONS) printf("<%ld> xor %d %d %d\n", task->task.pid, inst.R1, inst.R2, inst.R3);
}
void i9 (CPU_Task *task, Instruction inst )
{
  //mov
  task->context.reg[inst.R1] = task->context.reg[inst.R2];
  if(SHOWINSTRUCTIONS) printf("<%ld> mov %d %d\n", task->task.pid, inst.R1, inst.R2);
}
void iA (CPU_Task *task, Instruction inst )
{
  //cmp
  task->context.cc = task->context.reg[inst.R1] - task->context.reg[inst.R2];
  if(SHOWINSTRUCTIONS) printf("<%ld> cmp %d %d\n", task->task.pid, inst.R1, inst.R2);
}
void iB (CPU_Task *task, Instruction inst )
{
  //b
  task->context.pc = inst.AAAAAA;
  if(SHOWINSTRUCTIONS) printf("<%ld> b %d\n", task->task.pid, inst.AAAAAA);
}
void iC (CPU_Task *task, Instruction inst )
{
  //beq
  if(task->context.cc == 0) task->context.pc = inst.AAAAAA;
  if(SHOWINSTRUCTIONS) printf("<%ld> beq %d\n", task->task.pid, inst.AAAAAA);
}
void iD (CPU_Task *task, Instruction inst )
{
  //bgt
  if(task->context.cc > 0) task->context.pc = inst.AAAAAA;
  if(SHOWINSTRUCTIONS) printf("<%ld> bgt %d\n", task->task.pid, inst.AAAAAA);
}
void iE (CPU_Task *task, Instruction inst )
{
  //blt
  if(task->context.cc < 0) task->context.pc = inst.AAAAAA;
  if(SHOWINSTRUCTIONS) printf("<%ld> blt %d\n", task->task.pid, inst.AAAAAA);
}
void iF (CPU_Task *task, Instruction inst )
{
  //exit
  task->task.life = 0;
  if(SHOWINSTRUCTIONS) printf("<%ld> exit\n", task->task.pid);
}

Instruction_Table instructions[16] = { &i0, &i1, &i2, &i3, &i4, &i5, &i6, &i7, &i8, &i9, &iA, &iB, &iC, &iD, &iE, &iF };

void Execute(CPU_Task *t)
{
  unsigned int instruction = GetWord(t->context.pc, &(t->task.mm.pgb));
  t->context.pc+=4;

  Instruction i;
  uint8_t C = instruction >> 28;
  i.R1 = (instruction & 0x0FFFFFFF) >> 24;
  i.R2 = (instruction & 0x00FFFFFF) >> 20;
  i.R3 = (instruction & 0x000FFFFF) >> 16;
  i.AAAAAA = instruction & 0x00FFFFFF;

  instructions[C](t, i);
}

#endif //INSTRUCIONSET_H
