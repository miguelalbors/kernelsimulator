#ifndef QUEUE_H
#define QUEUE_H

/****************************************
  Lista FIFO de procesos en espera de
  ser atendidos por el dispatcher
****************************************/

#include "structs.h"

#include <stdlib.h>
#include <stdio.h>

int QUEUE_MAX = 30;

Queue queue;

void InitQueue()
{
  queue.data = malloc(sizeof(Task) * QUEUE_MAX);
  queue.front = 0;
  queue.rear = -1;
  queue.itemCount = 0;
  queue.max = QUEUE_MAX;
  printf("> Se inicializa la queue con %d huecos\n", QUEUE_MAX);
}

Task queuePeek()
{
   return queue.data[queue.front];
}

int queueIsEmpty()
{
   return (queue.itemCount == 0) ? 1 : 0;
}

int queueIsFull()
{
   return (queue.itemCount == queue.max) ? 1 : 0;
}

int queueSize()
{
   return queue.itemCount;
}

void queueInsert(Task task)
{
   if(!queueIsFull())
   {
      if(queue.rear == queue.max-1)
      {
         queue.rear = -1;
      }

      queue.data[++queue.rear] = task;
      queue.itemCount++;
   }
}

Task queueRemoveElement()
{
   Task data; data.pid = -1;
   if(queue.itemCount > 0)
   {
     data = queue.data[queue.front++];

     if(queue.front == queue.max)
     {
        queue.front = 0;
     }

     queue.itemCount--;
   }
   return data;
}
#endif //QUEUE_H
