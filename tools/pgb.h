#ifndef PAGINASTABLA_H
#define PAGINASTABLA_H

#include "structs.h"
#include "memory.h"
#include <stdio.h>
#include <stdlib.h>

PGB newPGB()
{
  PGB pgb;
  pgb.data = malloc(sizeof(PaginaRef) * 2);
  pgb.size = 2;
  pgb.count = 0;
  return pgb;
}

void _PGB_check_expansion(PGB *pgb)
{
  if(pgb->count == pgb->size)
  {
    PaginaRef *tmp = malloc(sizeof(PaginaRef) * (pgb->size << 1));
    for(int i = 0; i < pgb->size; i++)
    {
      tmp[i] = pgb->data[i];
    }

    free(pgb->data);
    pgb->data = tmp;
    pgb->size *= 2;
  }
}

void PGBInsert(PGB *pgb, PaginaRef page)
{
  _PGB_check_expansion(pgb);
  pgb->data[pgb->count] = page;
  pgb->count++;
}

int PGBGet(PGB *pgb, int dV)
{
  int i = dV >> 8;
  int dirPag = i << 8;

  for(i = 0; i < pgb->count; i++)
  {
    if(pgb->data[i].dV == dirPag)
    {
      break;
    }
  }

  return pgb->data[i].dF + (dV - dirPag);
}

void Free(int dF);
void PGBErase(PGB *pgb)
{
  for(int i = 0; i < pgb->count; i++)
  {
    Free(pgb->data[i].dF);
  }
}
#endif //PAGINASTABLA_H
