#ifndef PRIORITYQUEUE_H
#define PRIORITYQUEUE_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "structs.h"

struct node *head = NULL;
struct node *current = NULL;

void priorityQueueSort();

//insert link at the first location
void priorityQueueInsert(Task task, Context context, int data)
{
   //create a link
   struct node *link = (struct node*) malloc(sizeof(struct node));

   link->task = task;
   link->context = context;
   link->data = data;

   //point it to old first node
   link->next = head;

   //point first to new first node
   head = link;

   priorityQueueSort();
}

//delete first item
Task priorityQueueDeleteFirst(int *nice, Context *context)
{
   //save reference to first link
   struct node *tempLink = head;

   //mark next to first link as first
   head = head->next;

   //return the deleted link
   (*nice) = tempLink->data;
   (*context) = tempLink->context;
   return tempLink->task;
}

//is list empty
bool priorityQueueIsEmpty()
{
   return head == NULL;
}

int priorityQueueLength()
{
   int length = 0;
   struct node *current;

   for(current = head; current != NULL; current = current->next)
   {
      length++;
   }

   return length;
}

void priorityQueueNormaliceNices()
{
   int i, j, k, tempData = 0;
   struct node *current;
   struct node *next;

   int size = priorityQueueLength();
   k = size ;

   current = head;

   for(i = 0; i < size-1; i++) current = current->next;
   if(current->data < 0) tempData = -current->data * 2;

   current = head;
   for(i = 0; i < size; i++)
   {
     current->data += tempData;
     current = current->next;
   }
}
void priorityQueuePrintPriorityList()
{
   struct node *current = head;
   int size = priorityQueueLength();
   printf("< ");
   for (int i = 0 ; i < size ; i++)
   {
      printf("%ld ", current->task.pid);
      current = current->next;
   }
   printf(">\n");
}

void priorityQueueSort()
{
   int i, j, k, tempData;
   Task tempTask;
   Context tempContext;
   struct node *current;
   struct node *next;

   int size = priorityQueueLength();
   k = size ;

   for ( i = 0 ; i < size - 1 ; i++, k-- )
   {
      current = head;
      next = head->next;

      for ( j = 1 ; j < k ; j++ )
      {
         if ( current->data < next->data )
         {
            tempData = current->data;
            current->data = next->data;
            next->data = tempData;

            tempTask = current->task;
            current->task = next->task;
            next->task = tempTask;
            
            tempContext = current->context;
            current->context = next->context;
            next->context = tempContext;
         }

         current = current->next;
         next = next->next;
      }
   }
}
#endif //PRIORITYQUEUE_H
