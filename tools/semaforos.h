#ifndef SEMAFOROS_H
#define SEMAFOROS_H

#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>

sem_t oQueueAccess, queueAccess, timerTrigger, cpuAccess, ramAccess, pagetableAccess;

/****************************************
  Los diferentes semáforos que hacen falta
****************************************/

void InitSemaforos()
{
  if (sem_init(&oQueueAccess, 0, 1) == -1)
  {
    printf("Error creando oQueueAccess\n");
    exit(-1);
  }
  if (sem_init(&queueAccess, 0, 1) == -1)
  {
    printf("Error creando QueueAccess\n");
    exit(-1);
  }
  if (sem_init(&timerTrigger, 0, 0) == -1)
  {
    printf("Error creando TimerTrigger\n");
    exit(-1);
  }
  if (sem_init(&cpuAccess, 0, 1) == -1)
  {
    printf("Error creando cpuAccess\n");
    exit(-1);
  }
  if (sem_init(&ramAccess, 0, 1) == -1)
  {
    printf("Error creando ramAccess\n");
    exit(-1);
  }
  if (sem_init(&pagetableAccess, 0, 1) == -1)
  {
    printf("Error creando pagetableAccess\n");
    exit(-1);
  }
}

void SemaforoDown(sem_t *flag)
{
  sem_wait(flag);
}

void SemaforoUp(sem_t *flag)
{
  if (sem_post(flag) == -1)
  {
      printf("sem_post() failed\n");
  }
}

#endif //SEMAFOROS_H
