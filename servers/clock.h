#ifndef CLOCK_H
#define CLOCK_H

#include "../tools/cpu.h"
#include "../tools/semaforos.h"
extern sem_t timerTrigger;
extern sem_t cpuAccess;

extern void CPU_ClockStep();

int TIMER_TRIGGER = 100;

void* Clock()
{
  int trigger = 0;
  printf("> Se establece TIMER_TRIGGER a %d\n", TIMER_TRIGGER);

  while(1)
  {
    do
    {
      SemaforoDown(&cpuAccess);
      CPU_ClockStep();
      SemaforoUp(&cpuAccess);

      trigger++;
    } while(trigger < TIMER_TRIGGER);

    SemaforoUp(&timerTrigger);
    trigger = 0;
  }
}
#endif //CLOCK_H
