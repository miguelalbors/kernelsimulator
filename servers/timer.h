#ifndef TIMER_H
#define TIMER_H

#include "../tools/semaforos.h"
extern sem_t timerTrigger;

int ktime;

void* Timer()
{
  ktime = 0;
  while(1)
  {
    SemaforoDown(&timerTrigger);
    ktime++;
  }
}

#endif //TIMER_H
