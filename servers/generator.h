#ifndef GENERATOR_H
#define GENERATOR_H

#include "../tools/memory.h"
#include "../tools/pgb.h"
#include "../tools/queue.h"
#include "../tools/semaforos.h"
#include "../tools/structs.h"

#include <unistd.h>
#include <pthread.h>
#include <dirent.h>

extern sem_t queueAccess;

int GEN_MAX = 27;
int GEN_MIN = 15;
int GEN_SLP_MAX = 5;
int GEN_SLP_MIN = 1;
char *PROGFOLDER = "progs/";
int SHOWGENERATOR = 0;

int threads_created;
extern unsigned int PAGESIZE;

void newTask(Task *t)
{
  t->pid = 1 + rand()%5000;
  //t->life = (rand()%1000000 < 500000) ? rand()%10000 : rand()%10000000;
  t->priority = 1 + rand()%125;
  t->mm.pgb = newPGB();

  gettimeofday (&t->startedAt, 0);
}

void newProgram(Task *t, const char *rom_file)
{
  FILE *pf = fopen(rom_file, "r");
	if(NULL != pf)
	{
		fseek(pf, 0, SEEK_END);
		int tam = ftell(pf);

	  Palabra *rom = malloc(tam); //Reservo el array donde voy a volcar la rom
		fseek(pf, 0, SEEK_SET);
		fread(rom, tam, 1, pf); //vuelco la rom
		fclose(pf);

		int addrc = (rom[5] << 2) + (rom[6] << 1) + rom[7]; //Obtengo la dirección de .text
		int addrd = (rom[13] << 2) + (rom[14] << 1) + rom[15]; //Obtengo la dirección de .data
		int starting = 16;

		newTask(t); //Creo el proceso
    if(SHOWGENERATOR) printf("[Generador] Se carga el programa %s con el id %lu\n", rom_file, t->pid);

    // Asigno las direcciones de .text y .data
		  t->mm.code = (rom[5] << 2) + (rom[6] << 1) + rom[7];
		  t->mm.data = (rom[13] << 2) + (rom[14] << 1) + rom[15];

    //Calculo cuántas páginas va a ocupar
      int lines = tam-starting;
      int pages = (lines / (int)PAGESIZE) + ((lines % (int)PAGESIZE > 0)? 1 : 0);

    PaginaRef pr;

    //Reservo las páginas en memoria
      int aloc;
      for(int i = 0; i < pages; i++)
      {
        pr.dV = t->mm.code + (i*256);
        aloc = Alloc();
        if(aloc < 0)
        {
          if(SHOWGENERATOR) printf("[Generador] No se puede crear nuevo proceso. No hay espacio en memoria, se aborta\n");
          PGBErase(&(t->mm.pgb));
          t->pid = 0;
          return;
        }
    		pr.dF = (unsigned int)aloc;
        PGBInsert(&(t->mm.pgb), pr);
      }

    //Vuelco en memoria la rom
  		for(int i = 0; i < lines; i++)
  		{
  		  Set(i, rom[i+starting], &(t->mm.pgb));
  		}

      t->life = lines; //A falta de otro modo para calcular la vida del proceso
                       //se asigna como vida el número de líneas.
	}
	else
	{
		printf("[Error] El fichero indicado no puede abrirse.\n");
		exit(1);
	}

}

int CuentaProgs()
{
  int f = 0;

  struct dirent *de;
  DIR *dr = opendir(PROGFOLDER);

  if (dr == NULL)
  {
      printf("[Error] El directorio no existe\n");
      exit(0);
  }

  while ((de = readdir(dr)) != NULL)
  {
    if((strcmp(de->d_name, ".") != 0) && strcmp(de->d_name, "..") != 0)
    {
      f++;
    }
  }

  closedir(dr);
  return f;
}

char* GetProg(int id)
{
  struct dirent *de;
  DIR *dr = opendir(PROGFOLDER);

  if (dr == NULL)
  {
      printf("[Error] El directorio no existe\n" );
      exit(0);
  }

  int cont = 0;
  char *ret;
  while ((de = readdir(dr)) != NULL)
  {
    if((strcmp(de->d_name, ".") != 0) && strcmp(de->d_name, "..") != 0)
    {
      if(cont == id) break;
      else cont++;
    }
  }

  ret = de->d_name;
  closedir(dr);
  return ret;
}

void* Generator()
{
  int r = 0;
  Task t;
  int nF = CuentaProgs();
  int fileid;
  char *fid;

  if(SHOWGENERATOR) printf("[Generador] Se han encontrado %d programas\n", nF);

  while(1)
  {
    //Reutilizamos variable para establecer cantidad a generar
      r = GEN_MIN + rand()%(GEN_MAX - GEN_MIN);
      if(SHOWGENERATOR) printf("[Generador] Se van a crear %d procesos\n", r);

    for(; r > 0; r--)
    {
      SemaforoDown(&queueAccess);

      fileid = rand()%nF;
      fid = malloc(sizeof(char) * (strlen(PROGFOLDER) + strlen(GetProg(fileid))));
      strcpy(fid, PROGFOLDER);
      strcat(fid, GetProg(fileid));
      newProgram(&t, fid);
      free(fid);

      if(t.pid != 0)
      {
        if(!queueIsFull())
        {
          queueInsert(t);
          threads_created++;
        }
      }
      else
      {
        break;
      }

      SemaforoUp(&queueAccess);
    }

    //Establecemos tiempo para dormir
      r = GEN_SLP_MIN + rand()%(GEN_SLP_MAX - GEN_SLP_MIN);
      sleep(r);
  }
}

#endif //GENERATOR_H
