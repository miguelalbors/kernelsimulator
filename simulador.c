#include "tools/cpu.h"
#include "tools/queue.h"
#include "tools/semaforos.h"
#include "tools/memory.h"
#include "tools/instructionset.h"

#include "servers/clock.h"
#include "servers/timer.h"
#include "servers/generator.h"
#include "servers/scheduler.h"

#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>

extern int QUEUE_MAX;
extern int MAX_CORES;
extern int MAX_THREADS;
extern int MAX_CPUS;
extern int TIMER_TRIGGER;
extern int GEN_MIN;
extern int GEN_MAX;
extern int GEN_SLP_MIN;
extern int GEN_SLP_MAX;
extern int RAM_SIZE;
extern char *PROGFOLDER;
extern int SHOWINSTRUCTIONS;
extern int SHOWGENERATOR;
extern int SHOWSCHEDULER;
extern int SHOWMEMORY;

extern int threads_created;
extern int threads_finished;
extern double max_time;
extern double min_time;
int execTime = 0;

#define GENERATOR         0
#define SCHEDULER         1
#define TIMER             2
#define CLOCK             3
#define SERVERS_COUNT     4


int main(int argc, char *argv[])
{
  if(argc > 1)
  {
    for(int i = 1; i < argc; i++)
    {
      if(strcmp(argv[i], "--exec") == 0 || strcmp(argv[i], "-e") == 0)
      {
        execTime = atoi(argv[++i]);
      }
			else if(strcmp(argv[i], "--cores") == 0 || strcmp(argv[i], "-c") == 0)
			{
				MAX_CORES = atoi(argv[++i]);
			}
			else if(strcmp(argv[i], "--cpus") == 0)
			{
				MAX_CPUS = atoi(argv[++i]);
			}
			else if(strcmp(argv[i], "--threads") == 0 || strcmp(argv[i], "-t") == 0)
			{
				MAX_THREADS = atoi(argv[++i]);
			}
			else if(strcmp(argv[i], "--queue") == 0 || strcmp(argv[i], "-q") == 0)
			{
				QUEUE_MAX = atoi(argv[++i]);
			}
			else if(strcmp(argv[i], "--clock") == 0 || strcmp(argv[i], "-C") == 0)
			{
				TIMER_TRIGGER = atoi(argv[++i]);
			}
			else if(strcmp(argv[i], "--process") == 0 || strcmp(argv[i], "-p") == 0)
			{
				GEN_MIN = atoi(argv[++i]);
        GEN_MAX = atoi(argv[++i]);
			}
			else if(strcmp(argv[i], "--procslp") == 0)
			{
				GEN_SLP_MIN = atoi(argv[++i]);
        GEN_SLP_MAX = atoi(argv[++i]);
			}
			else if(strcmp(argv[i], "--ram") == 0 || strcmp(argv[i], "-r") == 0)
			{
				RAM_SIZE = atoi(argv[++i]);
			}
			else if(strcmp(argv[i], "--prog") == 0)
			{
        PROGFOLDER = malloc(sizeof(char)*strlen(argv[++i]));
				strcpy(PROGFOLDER, argv[i]);
			}
			else if(strcmp(argv[i], "--show") == 0 || strcmp(argv[i], "-s") == 0)
			{
        int l = strlen(argv[++i]);
        for(int j = 0; j < l; j++)
        {
          switch(argv[i][j])
          {
            case 'i':
            case 'I':
              SHOWINSTRUCTIONS = 1;
              break;
            case 'g':
            case 'G':
              SHOWGENERATOR = 1;
              break;
            case 's':
            case 'S':
              SHOWSCHEDULER = 1;
              break;
            case 'm':
            case 'M':
              SHOWMEMORY = 1;
              break;
          }
        }
			}
      else if(strcmp(argv[1], "--help") == 0 || strcmp(argv[i], "-h") == 0)
      {
        printf("Modo de uso: ./simulador [OPCIONES]\n");
        printf("Ejecuta el simulador\n");
        printf("Opciones:\n");
        printf("  -h, --help    muestra esta ayuda\n");
        printf("  -C, --clock   establece la cantidad de ciclos para que salte el timer\n");
        printf("  -c, --cores   establece el número de cores\n");
        printf("      --cpus    establece el número de CPUs\n");
        printf("  -e, --exec    establece el tiempo en segundos que durará la ejecución (0 = sin límite)\n");
        printf("  -p, --process establece el número mínimo y máximo de procesos que genera el generador\n");
        printf("      --procslp establece el tiempo mínimo y máximo en segundos que el generador dormirá\n");
        printf("      --prog    establece el directorio donde se encuentran las roms\n");
        printf("  -q, --queue   establece el número de huecos en la cola de espera\n");
        printf("  -r, --ram     establece el tamaño de la memoria\n");
        printf("  -s, --show    establece el modo de log [igsm]\n");
        printf("  -t, --threads establece el número de threads\n");
        printf("Por ejemplo:\n");
        printf("  ./simulador --cores 16 -t 6 -s i\n");
        exit(0);
      }
    }
  }

  if(execTime > 0) printf("> Se establece la duración de la ejecución en %d segundos\n", execTime);
  else printf("> No se establece la duración de la ejecución\n");

  //Establecemos una semilla
    srand(pthread_self());

  //Iniciamos las estructuras de datos
    InitCPUs();
    InitQueue();
    InitSemaforos();
    InitRam();

  //Iniciamos los servidores
    pthread_t servers[SERVERS_COUNT];

    pthread_create(&servers[CLOCK], NULL, Clock, NULL);
    pthread_create(&servers[TIMER], NULL, Timer, NULL);
    pthread_create(&servers[GENERATOR], NULL, Generator, NULL);
    pthread_create(&servers[SCHEDULER], NULL, ScheduleServer, NULL);

  do {
    sleep(1);
    execTime--;
  } while(execTime != 0);

  printf("-----------------------------------------------------------------------\n");
  printf("%d tareas creadas\n", threads_created);
  printf("%d tareas terminadas\n", threads_finished);
  printf("La tarea que más ha tardado en ejecutarse ha tardado %f segundos\n", max_time);
  printf("La tarea que menos ha tardado en ejecutarse ha tardado %f segundos\n", min_time);
  printf("-----------------------------------------------------------------------\n");
  return 0;
}
