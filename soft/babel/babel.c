#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

char* str_split(char* a_str, const char a_delim, int ret)
{
    char** result    = 0;
    size_t count     = 0;
    char* tmp        = a_str;
    char* last_comma = 0;
    char delim[2];
    delim[0] = a_delim;
    delim[1] = 0;
    size_t elem = 0;

    /* Count how many elements will be extracted. */
    while (*tmp)
    {
        if (a_delim == *tmp)
        {
            count++;
            last_comma = tmp;
        }
        tmp++;
    }

    elem = count;

    /* Add space for trailing token. */
    count += last_comma < (a_str + strlen(a_str) - 1);

    /* Add space for terminating null string so caller
       knows where the list of returned strings ends. */
    count++;

    result = malloc(sizeof(char*) * count);

    if (result)
    {
        size_t idx  = 0;
        char* token = strtok(a_str, delim);

        while (token)
        {
            *(result + idx++) = strdup(token);
            token = strtok(0, delim);
        }
        *(result + idx) = 0;
    }

    if(elem > 0)
    {
      return result[elem - ret];
    }
    else
    {
      return "";
    }
}

uint8_t getHex(char c)
{
  uint8_t ret;
  switch(c)
  {
    case '0':
      ret = 0x00;
      break;
    case '1':
      ret = 0x01;
      break;
    case '2':
      ret = 0x02;
      break;
    case '3':
      ret = 0x03;
      break;
    case '4':
      ret = 0x04;
      break;
    case '5':
      ret = 0x05;
      break;
    case '6':
      ret = 0x06;
      break;
    case '7':
      ret = 0x07;
      break;
    case '8':
      ret = 0x08;
      break;
    case '9':
      ret = 0x09;
      break;
    case 'A':
      ret = 0x0A;
      break;
    case 'B':
      ret = 0x0B;
      break;
    case 'C':
      ret = 0x0C;
      break;
    case 'D':
      ret = 0x0D;
      break;
    case 'E':
      ret = 0x0E;
      break;
    case 'F':
      ret = 0x0F;
      break;
  }

  return ret;
}

int main(int argc, char *argv[])
{
  int i, files;
  if(argc == 1)
  {
    printf("argumentos requeridos\n");
    return 0;
  }

  for(files = argc; files > 1; files--)
  {
    FILE *pf = fopen(argv[files-1], "r");
    if(NULL != pf)
  	{
      fseek(pf, 0, SEEK_END);
  		int tam = ftell(pf);
      char *code = malloc(sizeof(char)*tam);
  		fseek(pf, 0, SEEK_SET);
  		fread(code, tam, 1, pf);
  		fclose(pf);

      char *filename = str_split(argv[files-1], '/', 0);
      filename = str_split(filename, '.', 1);
      strcat(filename, ".elf");

      FILE *pf = fopen(filename, "wb");
      // 0 - 5 .text 2E74657874
      uint8_t dato;
      dato = 0x2E;
      fwrite(&dato, 1, sizeof(uint8_t), pf);
      dato = 0x74;
      fwrite(&dato, 1, sizeof(uint8_t), pf);
      dato = 0x65;
      fwrite(&dato, 1, sizeof(uint8_t), pf);
      dato = 0x78;
      fwrite(&dato, 1, sizeof(uint8_t), pf);
      dato = 0x74;
      fwrite(&dato, 1, sizeof(uint8_t), pf);

      // 6 - 11 code[i]
      for(i = 6; i < 12; i+=2)
      {
        dato = getHex(code[i]);
        dato <<= 4;
        dato += getHex(code[i+1]);
        fwrite(&dato, 1, sizeof(uint8_t), pf);
      }

      // 7 - 7 \n

      // 8 - 13 .data 2E64617661
      dato = 0x2E;
      fwrite(&dato, 1, sizeof(uint8_t), pf);
      dato = 0x64;
      fwrite(&dato, 1, sizeof(uint8_t), pf);
      dato = 0x61;
      fwrite(&dato, 1, sizeof(uint8_t), pf);
      dato = 0x74;
      fwrite(&dato, 1, sizeof(uint8_t), pf);
      dato = 0x61;
      fwrite(&dato, 1, sizeof(uint8_t), pf);

      // 18 - 14 code[i]
      for(i = 19; i < 25; i+=2)
      {
        dato = getHex(code[i]);
        dato <<= 4;
        dato += getHex(code[i+1]);
        fwrite(&dato, 1, sizeof(uint8_t), pf);
      }

      dato = 0;
      int par = 1;
      for(i = 25; i < tam; i++)
      {
        if(code[i] != '\n')
        {
          dato += getHex(code[i]);
          if(par % 2 == 0)
          {
            fwrite(&dato, 1, sizeof(uint8_t), pf);
            dato = 0x00;
          }
          else
          {
            dato <<= 4;
          }

          par++;
        }
      }

      if(par%2 != 0)
      {
        dato <<= 4; // dato = dato << 1;
        dato += 0x00;
      }

      fclose(pf);
    }
  }

  return 1;
}
